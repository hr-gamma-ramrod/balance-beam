# Daily Journal

## 1/17/2024
Team notes:
    - Knocked out several quick linear tickets including env file creation and env variable creation.
    - Became stuck with a bug during psycogp installation and connecting to the database. We worked through the problem for several hours befor realizing the password complexity was throwing the code off due to special characters creating conflict.
    - We didn’t get as far as we wanted to, but seem to be in a good spot. We should be able to get our tables complete tomorrow, stub out our endpoints, and get a solid start on authorization.
Personal notes:
    - Committed two changes (env creation and env variable creation).
## 1/19/2024
Team notes:
    - Knocked out most of the backend authorization
    - Became stuck with a bug during the final stages of authorization but were able to get through it when it was discoverd that we had the wrong paramters inputed into the function.
Personal notes:
    - Committed two changes (Auth-1 and Auth-5)
## 1/20/2024
Team Notes:
    - Me briefly to finish up backend authorization.
    - Verified in Swagger that authorization was required to "get one user," so the plan is to build the Workout functions and use authorization with all of them.
Personal Notes:
    - Committed the final auth change of the day.
    - Added the user Authorizations.
## 1/22/2024
Team Notes:
    - Created the paths for getting single workout.
    - Implemented the create workout path and enpoint.
Personal Notes:
    - Added leaderboard to both the queries and the routers.
## 1/23/2024
Team Notes:
    - Began formatting the ReadMe file.
    - Updated enpoint for the get leaderboard API call.
Personal Notes:
    - Updated the "start_time" field in the workout and leaderboard models to "date" throughout the code.
    - Updated the readme file with API endpoints and JSON formatting.
## 1/24/2024
Team Notes:
    -Edited the Workout table to make user_id a foreign key.
    -Had a conversation about creating test data.
Personal Notes:
    -Created the login page with username and password.
    -Created the dashboard page without an API call yet.
## 1/25/2024
Team Notes:
    - Team worked through first stages of front end auth.
    - Updated readme.
    - User stories and testimonils.
Personal Notes:
    - Spent a better part of the day trying to work through a CORS issue for the leaderboard.
      I found later on that it was an authentication issue, rather than an issue with our CORS
      settings. I found this in the main.py file within the API folder.
## 1/26/2024
Team Notes:
    - Got stuck for quite some time with front-end authorization. 
    - Managed to get registration and login working prior to COB.
Personal Notes:
    - Spent another chunk of time trying to fix the API call in the leaderboard
    - There were two key areas where the mistakes were happening:
        - The credentials were incorrect as an input for the API.
        - The function called for a parameter we were not including.
## 1/29/2024
Team Notes:
    - Worked on the timer for the create-workout page.
    - Worked on basivc nav-bar elements.
Personal Notes:
    - The API call for Leaderboard is now operational and we figured how to add it sitewide.
    - Worked on the create workout query.
## 1/30/2024
Team Notes:
    - Worked on the carousel for the home page.
    - Implemented frontend auth in more pages.
Personal Notes:
    - Implemented the countdown timer on the start page.
    - Implemented link logic sitewide. All current links are not directed appropriately.
## 1/31/2024
Team Notes:
    - Worked on CSS for several of the pages including the home, sign in and register pages, and the create-workout page.
    - Broke off into pairs in the afternoon with one pair handling links and the other pair working on CSS.
    - Ammended the create-workout page to include a modal related to the "end workout early" button.
Personal Notes:
    - Worked with Brendan for a good majority of the day to create paths for all of the links in the app.
    - Came to a temporary solution for the redirect issue by putting a timer on the redirect functions.
    - Worked no some CSS styling with Brendan in order to differentiate between complete and incomplete workouts in the user dashboard.
## 2/1/2024
Team Notes:
    - Updated readme file again.
    - Instructions page and create-workout page now completely functional.
Personal Notes:
    - Worked through login and signup issues.
## 2/5/2024
Team Notes:
    - Worked on styling for the user dashboard and leaderboard.
Personal Notes:
    - Added 404 Not Found page with link to the home page.
    - Added timer to the login page for the redirect so that users would not see error message.
## 2/6/2024
Team Notes:
    - Team all worked on unit tests and error handling.
    - Downloaded tremor for building graphs.
Personal Notes:
    - Created unit test for get_leaderboard.
## 2/7/2024
Team Notes:
    - Worked through some issues with the clock on the create-workout page.
    - Worked thorugh the last unit test - this one being a post.
Personal Notes:
    - Worked with Brendan on the register function refactoring.
    - Set up graphs on the user dashboard page.
## 2/8/2024
Team Notes:
    - Put finishing touches throughout site.
    - Worked through a few timing issues with the create-workout page.
    - Did a run through of the site.
    - Built the env sample page.
Personal Notes:
    - Wrote a new login function.
    - Performed a complete site startup using the readme file.
