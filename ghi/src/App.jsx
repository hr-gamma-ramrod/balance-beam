import { useEffect, useState } from "react"
import { BrowserRouter, Routes, Route } from "react-router-dom"
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react"
import { API_HOST } from "./main"
import "./App.css"
import HomePage from "./Components/HomePage"
import SignUpPage from "./Components/SignUpPage"
import LoginPage from "./Components/LoginPage"
import Leaderboard from "./Components/Leaderboard"
import DashboardPage from "./Components/DashboardPage"
import Instructions from "./Components/Instructions"
import Nav from "./Components/NavBar"
import Countdown from "./Components/CountdownPage"
import CreateWorkout from "./Components/CreateWorkout"
import PageNotFound from "./Components/NotFound404"
import fetchData from "./Common/fetchData"


function App() {
    const [userData, setUserData] = useState()
    const { token } = useAuthContext()

    useEffect(() => {
        const tokenURL = `${API_HOST}/token`
        const fetchConfig = {
            credentials: "include",
        }
        fetchData(tokenURL, fetchConfig, setUserData)
    }, [token])

    return (
        <BrowserRouter>
            <Nav username={userData && userData.user.username} />
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/signup" element={<SignUpPage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/leaderboard" element={<Leaderboard />} />
                <Route path="/dashboard" element={<DashboardPage />} />
                <Route path="/instructions" element={<Instructions />} />
                <Route path="/start" element={<Countdown />} />
                <Route path="/workout" element={<CreateWorkout />} />
                <Route path="*" element={<PageNotFound />} />
            </Routes>
        </BrowserRouter>
    )
}

export default App
