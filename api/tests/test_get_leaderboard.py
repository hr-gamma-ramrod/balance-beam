from fastapi.testclient import TestClient
from main import app
from models.models import LeaderboardOut
from queries.workouts import WorkoutsQueries
from authenticator import authenticator
from datetime import date


client = TestClient(app)


class MockGetLeaderboard:
    """
    Mock version of the workouts queries from `api/queries/workouts.py`
    """
    def get_leaderboard(self):
        return [
            LeaderboardOut(
                date=date(2023, 11, 1),
                run_1=23,
                set_1=23,
                set_2=23,
                set_3=23,
                set_4=23,
                set_5=23,
                set_6=23,
                set_7=23,
                set_8=23,
                set_9=23,
                set_10=23,
                run_2=12,
                duration=32,
                username="bob",
                id=1,
                is_completed=True,
                user_id=11
            )
        ]


def mock_get_current_account_data():
    """
    Mocking get current account data
    """
    return {"id": 11}


def test_get_leaderboard():
    """
    Test Function to GET the leaderboard
    """
    app.dependency_overrides[
            authenticator.get_current_account_data
        ] = mock_get_current_account_data
    app.dependency_overrides[WorkoutsQueries] = MockGetLeaderboard
    response = client.get("api/workouts/leaderboard")
    assert response.status_code == 200
    leaderboard = response.json()
    assert len(leaderboard) == 1
    app.dependency_overrides = {}
