from fastapi.testclient import TestClient
from main import app
from queries.workouts import WorkoutOut, WorkoutsQueries
from authenticator import authenticator
from datetime import date


client = TestClient(app)


class MockWorkoutQueries:
    """
    mock version of workout queries
    """
    def get_all_user_workouts(self, user_id: int):
        return [
            WorkoutOut(
                date=date(2024, 2, 1),
                run_1=10,
                set_1=3,
                set_2=222,
                set_3=2,
                set_4=3,
                set_5=3,
                set_6=3,
                set_7=3,
                set_8=3,
                set_9=3,
                set_10=3,
                run_2=3,
                user_id=5,
                is_completed=False,
                id=2
            ),
            WorkoutOut(
                date=date(2024, 2, 1),
                run_1=10,
                set_1=3,
                set_2=222,
                set_3=2,
                set_4=3,
                set_5=3,
                set_6=3,
                set_7=3,
                set_8=3,
                set_9=3,
                set_10=3,
                run_2=3,
                user_id=5,
                is_completed=False,
                id=2
            ),
        ]


def mock_get_current_account_data():
    """
    Mocking get current account data
    """
    return {"id": 1}


def test_get_all_user_workouts():
    """
    test to get all user workouts
    """
    app.dependency_overrides[
            authenticator.get_current_account_data
        ] = mock_get_current_account_data
    app.dependency_overrides[WorkoutsQueries] = MockWorkoutQueries
    response = client.get("/api/workouts/mine")
    assert response.status_code == 200
    workouts = response.json()
    assert len(workouts) == 2
    app.dependency_overrides = {}
